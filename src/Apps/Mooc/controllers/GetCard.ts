import {Controller} from "./Controller";
import {Request, Response} from "lambda-api";
import {CardRetriever} from "../../../Context/Mooc/Card/application/get/CardRetriever";
import {
  PostgreSQLCardRepository
} from "../../../Context/Mooc/Card/infraestructure/persistence/PostgreSQLCardRepository";
import {PostgreSQLFactory} from "../../../Context/Shared/infraestructure/persistence/postgreSQL/PostgreSQLFactory";
import {
  PostgreSQLConfigFactory
} from "../../../Context/Mooc/Shared/infraestructure/persistance/postgreSQL/PostgreSQLConfigFactory";
import {RedisTokenRepository} from "../../../Context/Mooc/Token/infraestructure/persistence/RedisTokenRepository";
import {RedisFactory} from "../../../Context/Shared/infraestructure/persistence/redis/RedisFactory";
import {RedisConfigFactory} from "../../../Context/Mooc/Shared/infraestructure/persistance/redis/RedisConfigFactory";
import {TokenRetriever} from "../../../Context/Mooc/Token/application/get/TokenRetriever";
import {BuildResponse} from "../../../Context/Shared/infraestructure/response/BuildResponse";

export class GetCard implements Controller {
  async run(req: Request, res: Response): Promise<void> {
    try {
      const authorizationHeader = req.headers.authorization;
      const token = authorizationHeader?.substring('Bearer '.length) ?? null;
      const cardRepository = new PostgreSQLCardRepository(PostgreSQLFactory.createClient(PostgreSQLConfigFactory.createConfig()))

      const tokenRepository = new RedisTokenRepository(RedisFactory._createClient(RedisConfigFactory.createConfig()))

      const tokeRetriever = new TokenRetriever(tokenRepository)
      const cardRetriever = new CardRetriever(cardRepository, tokeRetriever)

      const card = await cardRetriever.run({
        token
      })

      const body = {
        card_number: card?.card_number ?? null,
        email: card?.email ?? null,
        expiration_year: card?.expiration_year ?? null,
        expiration_month: card?.expiration_month ?? null
      }

      return BuildResponse.run(body, res)
    } catch (e: Error | any) {
      console.log({status: e.status})
      return BuildResponse.run({status: e.status, message: e.message}, res)
    }
  }
}
import {Controller} from "./Controller";
import {Request, Response} from "lambda-api";
import {CardTokenizer} from "../../../Context/Mooc/Card/application/Tokenize/CardTokenizer";
import {
  PostgreSQLCardRepository
} from "../../../Context/Mooc/Card/infraestructure/persistence/PostgreSQLCardRepository";
import {TokenCreator} from "../../../Context/Mooc/Token/application/create/TokenCreator";
import {
  RedisTokenRepository
} from "../../../Context/Mooc/Token/infraestructure/persistence/RedisTokenRepository";
import {TokenRemover} from "../../../Context/Mooc/Token/application/remove/TokenRemover";
import {PostgreSQLFactory} from "../../../Context/Shared/infraestructure/persistence/postgreSQL/PostgreSQLFactory";
import {
  PostgreSQLConfigFactory
} from "../../../Context/Mooc/Shared/infraestructure/persistance/postgreSQL/PostgreSQLConfigFactory";
import {RedisFactory} from "../../../Context/Shared/infraestructure/persistence/redis/RedisFactory";
import {RedisConfigFactory} from "../../../Context/Mooc/Shared/infraestructure/persistance/redis/RedisConfigFactory";
import {BuildResponse} from "../../../Context/Shared/infraestructure/response/BuildResponse";
import httpStatus from "http-status";


type TokenCreateRequest = Request & {
  body: {
    card_number: string,
    cvv: string,
    expiration_month: string,
    expiration_year: string,
    email: string
  }
}

export class TokenizeCreditCard implements Controller {
  async run(req: Request & TokenCreateRequest, res: Response): Promise<void> {
    try {

      if(!req.body){
        return BuildResponse.run({status: httpStatus.BAD_REQUEST, message: "Tarjeta no contiene atributos"}, res)
      }

      const cardRepository = new PostgreSQLCardRepository(PostgreSQLFactory.createClient(PostgreSQLConfigFactory.createConfig()))
      const tokenRepository = new RedisTokenRepository(RedisFactory._createClient(RedisConfigFactory.createConfig()))
      const tokenCreator = new TokenCreator(tokenRepository)
      const tokenRemover = new TokenRemover(tokenRepository)
      const cardTokenizer = new CardTokenizer(
        cardRepository,
        tokenCreator,
        tokenRemover
      )

      const response = await cardTokenizer.run({
        card_number: req.body.card_number,
        email: req.body.email,
        expiration_year: req.body.expiration_year,
        cvv: req.body.cvv,
        expiration_month: req.body.expiration_month
      })

      const body = {
        token: response.token
      }
      return BuildResponse.run(body, res)
    } catch (e: Error | any) {
      return BuildResponse.run({status: e.status, message: e.message}, res)
    }
  }
}
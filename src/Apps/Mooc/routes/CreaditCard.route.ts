import {API} from "lambda-api";
import {TokenizeCreditCard} from "../controllers/TokenizeCreditCard";
import {GetCard} from "../controllers/GetCard";

const register = (api: API): void => {
  const controllerTokenizeCard = new TokenizeCreditCard()
  const controllerGetCard = new GetCard()
  api.post(
    "/",
    controllerTokenizeCard.run
  )
  api.get(
    "/",
    controllerGetCard.run
  )
}

export default register
import {Client} from "pg";

export class PostgreSqlRepository {
  constructor(private _client: Promise<Client>) {
  }

  protected async client(): Promise<Client> {
    return await this._client
  }

  protected async query(query: string, values: string[]): Promise<any[]> {
    return new Promise((resolve, reject) => {
      (async () => {
        try {
          const result = await (await this._client).query(query, values);
          resolve(result.rows);
        } catch (error) {
          reject(error);
        }
      })();
    });
  }

  protected async close() {
    await (await this._client).end()
  }
}
import {PostgreSQLConfig} from "./PostgreSQLConfig";
import {Client} from "pg"

export class PostgreSQLFactory {
  private static instance: Client;

  static async createClient(config: PostgreSQLConfig): Promise<Client> {
    try {
      if (!PostgreSQLFactory.instance) {
        const client = new Client({
          user: config.user,
          host: config.host,
          database: config.database,
          password: config.password,
          port: config.port,
          ssl: {
            rejectUnauthorized: false
          }
        })
        await client.connect()
        PostgreSQLFactory.instance = client
      }
      return PostgreSQLFactory.instance
    } catch (e) {
      console.log(">Error instancia postgres: ", e)
      throw new Error("Internar error")
    }
  }
}
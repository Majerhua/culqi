export interface PostgreSQLConfig {
  user: string,
  host: string,
  database: string,
  password: string,
  port: number
}
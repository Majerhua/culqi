import {RedisClientType} from "redis";

export class RedisRepository {
  constructor(private _client: Promise<RedisClientType>) {
  }

  protected async set(key: string, value: string, expirationTime: number = 900) {
    await (await this._client).set(key, value)
    await (await this._client).expire(key, expirationTime)
  }

  protected async get(key: string): Promise<string | null> {
    return (await this._client).get(key)
  }

  protected async delete(key: string) {
    await (await this._client).del(key)
  }

  protected async close() {
    await (await this._client).disconnect()
  }
}
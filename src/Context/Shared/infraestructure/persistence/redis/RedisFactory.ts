import {createClient, RedisClientType} from "redis";
import {RedisConfig} from "./RedisConfig";

export class RedisFactory {
  private static instance: RedisClientType

  static async _createClient(config: RedisConfig): Promise<RedisClientType> {
    try {
      if (!RedisFactory.instance) {
        const client: RedisClientType = createClient(config)
        client.on('error', err => console.log('Redis Client Error', err));
        await client.connect()
        RedisFactory.instance = client
      }

      return RedisFactory.instance

    } catch (e) {
      console.log(">Error instancia redis: ", e)
      throw new Error("Internar error")
    }
  }
}
import {StringValueObject} from "../../../Shared/domain/valueObject/StringValueObject";
import {InvalidArgumentError} from "../../../Shared/domain/valueObject/InvalidArgumentError";

export class CardEmail extends StringValueObject {
  private readonly MAX_SIZE = 100
  private readonly MIN_SIZE = 5
  private readonly RegexEmail: RegExp = /^[a-zA-Z0-9._%+-]+@(gmail\.com|hotmail\.com|yahoo\.es)$/

  constructor(value: string) {
    super(value);
    this.ensureFormat(value)
    this.ensureThatSizeIsGreater5AndLessThan100(value)
  }

  private ensureThatSizeIsGreater5AndLessThan100(value: string) {
    if (value.length < this.MIN_SIZE) {
      throw new InvalidArgumentError(`Email no puede ser menor a ${this.MIN_SIZE} caracteres`)
    }

    if (value.length > this.MAX_SIZE) {
      throw new InvalidArgumentError(`Email no puede ser mayor a ${this.MAX_SIZE} caracteres`)
    }
  }

  private ensureFormat(value: string) {
    const result: boolean = this.RegexEmail.test(value)
    if (!result) {
      throw new InvalidArgumentError("Email tiene formato inválido")
    }
  }
}
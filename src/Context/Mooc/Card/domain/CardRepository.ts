import {Card} from "./Card";
import {CardNumber} from "./CardNumber";
import {TokenId} from "../../Token/domain/TokenId";

export interface CardRepository {
  save(card: Card): Promise<void>

  findByCardNumber(cardNumber: CardNumber): Promise<null | Card>

  updateTokenByCardNumber(cardNumber: CardNumber, token: TokenId): Promise<void>

  findByToken(tokenId: TokenId): Promise<Card | null>
}
import {StringValueObject} from "../../../Shared/domain/valueObject/StringValueObject";
import {InvalidArgumentError} from "../../../Shared/domain/valueObject/InvalidArgumentError";

export class CardNumber extends StringValueObject {
  private readonly regexOnlyNumber: RegExp = /^[0-9]+$/
  private readonly MIN_SIZE = 13
  private readonly MAX_SIZE = 16
  readonly value: string

  constructor(value: string) {
    super(value)
    this.value = this.replaceSpaceCharacter(value)
    this.ensureSize(this.value)
    this.ensureIsOnlyNumber(this.value)
    this.ensureIsValidCardNumber(this.value)
  }

  private ensureIsValidCardNumber(value: string): void {
    const digits: number[] = value.split('').map(Number);
    let isOdd: boolean = true
    let totalSum: number = 0

    while (digits.length > 0) {
      const currentDigit = digits.pop() ?? 0
      if (isOdd) {
        totalSum += currentDigit
        isOdd = false
      } else {
        let doubleDigit = currentDigit * 2
        if (doubleDigit > 9) {
          doubleDigit -= 9;
        }
        totalSum += doubleDigit
        isOdd = true
      }
    }
    if (totalSum % 10 !== 0) {
      throw new InvalidArgumentError("Número de tarjeta invalida")
    }
  }

  private ensureIsOnlyNumber(input: string): void {
    const response: boolean = this.regexOnlyNumber.test(input)
    if (!response) {
      throw new InvalidArgumentError("Número de tarjeta solo acepta números")
    }
  }

  private replaceSpaceCharacter(input: string): string {
    return input.replace(/[\s-]/g, '');
  }

  private ensureSize(input: string) {
    if (input.length < this.MIN_SIZE || input.length > this.MAX_SIZE) {
      throw new InvalidArgumentError(`Número de dígitos tiene que ser mayor a ${this.MIN_SIZE} y menor a ${this.MAX_SIZE}`)
    }
  }

}
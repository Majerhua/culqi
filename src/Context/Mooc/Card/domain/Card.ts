import {CardNumber} from "./CardNumber";
import {CardCCV} from "./CardCCV";
import {CardExpirationMonth} from "./CardExpirationMonth";
import {CardExpirationYear} from "./CardExpirationYear";
import {CardEmail} from "./CardEmail";

export class Card {
  token: string
  readonly card_number: CardNumber
  readonly ccv: CardCCV
  readonly expiration_month: CardExpirationMonth
  readonly expiration_year: CardExpirationYear
  readonly email: CardEmail

  constructor(token: string, cardNumber: CardNumber, ccv: CardCCV, expirationMoth: CardExpirationMonth, expirationYear: CardExpirationYear, email: CardEmail) {
    this.token = token
    this.card_number = cardNumber
    this.ccv = ccv
    this.expiration_month = expirationMoth
    this.expiration_year = expirationYear
    this.email = email
  }

  static fromPrimitive(plainDate: {
    token: string,
    card_number: string,
    ccv: string,
    expiration_month: string,
    expiration_year: string,
    email: string
  }): Card {
    return new Card(
      plainDate.token,
      new CardNumber(plainDate.card_number),
      new CardCCV(plainDate.ccv),
      new CardExpirationMonth(plainDate.expiration_month),
      new CardExpirationYear(plainDate.expiration_year),
      new CardEmail(plainDate.email)
    )
  }

  toPrimitives(): any {
    return {
      token: this.token,
      card_number: this.card_number.value,
      ccv: this.ccv.value,
      expiration_month: this.expiration_month.value,
      expiration_year: this.expiration_year.value,
      email: this.email.value
    };
  }

  setToken(token: string) {
    this.token = token
  }
}
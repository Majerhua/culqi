import {InvalidArgumentError} from "../../../Shared/domain/valueObject/InvalidArgumentError";

export class CardCCV {
  readonly value: string
  private readonly MIN_SIZE: number = 3
  private readonly MAX_SIZE: number = 4
  private readonly regexOnlyNumber: RegExp = /^[0-9]+$/

  constructor(value: string) {
    this.value = value
    this.ensureIsOnlyNumber(this.value)
    this.ensureSize(this.value)
  }

  private ensureSize(value: string): void {
    if (value.length !== this.MIN_SIZE && value.length !== this.MAX_SIZE) {
      throw new InvalidArgumentError(`El tamaño de cvv tiene que ser entre ${this.MIN_SIZE} a ${this.MAX_SIZE} dígito`)
    }
  }

  private ensureIsOnlyNumber(input: string): void {
    const response: boolean = this.regexOnlyNumber.test(input)
    if (!response) {
      throw new InvalidArgumentError("Cvv solo permite números")
    }
  }
}
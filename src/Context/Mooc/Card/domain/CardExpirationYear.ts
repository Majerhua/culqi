import {StringValueObject} from "../../../Shared/domain/valueObject/StringValueObject";
import {InvalidArgumentError} from "../../../Shared/domain/valueObject/InvalidArgumentError";

export class CardExpirationYear extends StringValueObject {
   private readonly regexOnlyNumber: RegExp = /^[0-9]+$/
   private readonly currentlyYear: number = (new Date()).getFullYear()
   private readonly MAX_VALID_YEAR: number = 5

   constructor(value: string) {
      super(value);
      this.ensureIsOnlyNumber(value)
      this.ensureIsTheDateInRange(value)
   }

   private ensureIsOnlyNumber(input: string): void {
      const response: boolean = this.regexOnlyNumber.test(input)
      if (!response) {
         throw new InvalidArgumentError("Año de vencimieto solo admite números")
      }
   }

   private ensureIsTheDateInRange(value: string) {
      const currentlyYearNumber: number = Number(value)

      if (currentlyYearNumber < this.currentlyYear) {
         throw new InvalidArgumentError("Tarjeta vencida")
      }

      if (currentlyYearNumber - this.currentlyYear >= this.MAX_VALID_YEAR) {
         throw new InvalidArgumentError("Año de vencimiento fuera de rango")
      }
   }

}
import {StringValueObject} from "../../../Shared/domain/valueObject/StringValueObject";
import {InvalidArgumentError} from "../../../Shared/domain/valueObject/InvalidArgumentError";

export class CardExpirationMonth extends StringValueObject {
  private readonly regexOnlyNumber: RegExp = /^[0-9]+$/
  private readonly MIN_VALUE = 1
  private readonly MAX_VALUE = 12

  constructor(value: string) {
    super(value)
    this.ensureIsOnlyNumber(value)
    this.ensureIsInRange1to12(value)
  }

  private ensureIsOnlyNumber(input: string): void {
    const response: boolean = this.regexOnlyNumber.test(input)
    if (!response) {
      throw new InvalidArgumentError("Mes de vencimiento solo admite números")
    }
  }

  private ensureIsInRange1to12(value: string) {
    const valueNumber: number = Number(value)
    if (valueNumber < this.MIN_VALUE || valueNumber > this.MAX_VALUE) {
      throw new InvalidArgumentError(`Mes de vencimiento tiene que estar en el rango de ${this.MIN_VALUE} a ${this.MAX_VALUE}`)
    }
  }
}
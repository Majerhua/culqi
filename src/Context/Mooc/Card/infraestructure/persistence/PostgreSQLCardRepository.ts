import {CardRepository} from "../../domain/CardRepository";
import {CardNumber} from "../../domain/CardNumber";
import {Card} from "../../domain/Card";
import {PostgreSqlRepository} from "../../../../Shared/infraestructure/persistence/postgreSQL/PostgreSqlRepository";
import {TokenId} from "../../../Token/domain/TokenId";

export class PostgreSQLCardRepository extends PostgreSqlRepository implements CardRepository {
  async save(card: Card): Promise<void> {
    const query = `
      INSERT INTO card (token, card_number, ccv, expiration_month, expiration_year, email)
      VALUES ($1, $2, $3, $4, $5, $6)
      RETURNING id
    `;

    const values = [
      card.token,
      card.card_number.value,
      card.ccv.value,
      card.expiration_month.value,
      card.expiration_year.value,
      card.email.value,
    ];
    await this.query(query, values)
  }

  async findByCardNumber(cardNumber: CardNumber): Promise<Card | null> {
    const query = `
      SELECT * FROM card
      WHERE card_number = $1
      limit 1
    `;

    const values = [cardNumber.value];
    const result = await this.query(query, values);
    return result && result[0] ? Card.fromPrimitive({
      token: String(result[0].token),
      card_number: String(result[0].card_number),
      ccv: String(result[0].ccv),
      email: String(result[0].email),
      expiration_year: String(result[0].expiration_year),
      expiration_month: String(result[0].expiration_month)
    }) : null
  }

  async updateTokenByCardNumber(cardNumber: CardNumber, token: TokenId): Promise<void> {
    const query = `
      UPDATE card
      SET token = $1
      WHERE card_number = $2
    `;

    const values = [token.value, cardNumber.value];
    await this.query(query, values)
  }

  async findByToken(tokenId: TokenId): Promise<Card | null> {
    const query = `
      SELECT * FROM card
      WHERE token = $1
      limit 1
    `;

    const values = [tokenId.value];

    const result = await this.query(query, values);
    return !result && result[0] ? null : Card.fromPrimitive({
      token: String(result[0].token),
      card_number: String(result[0].card_number),
      ccv: String(result[0].ccv),
      email: String(result[0].email),
      expiration_year: String(result[0].expiration_year),
      expiration_month: String(result[0].expiration_month)
    })
  }

  async end() {
    await this.close()
  }
}
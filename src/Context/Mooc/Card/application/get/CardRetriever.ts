import {CardRepository} from "../../domain/CardRepository";
import {TokenRetriever} from "../../../Token/application/get/TokenRetriever";
import {TokenId} from "../../../Token/domain/TokenId";
import {InvalidArgumentError} from "../../../../Shared/domain/valueObject/InvalidArgumentError";
import httpStatus from "http-status";
import {Card} from "../../domain/Card";

export class CardRetriever {
  private readonly cardRepository: CardRepository
  private readonly tokenRetriever: TokenRetriever

  constructor(cardRepository: CardRepository, tokenRetriever: TokenRetriever) {
    this.cardRepository = cardRepository
    this.tokenRetriever = tokenRetriever
  }

  async run(params: { token: string | null }): Promise<Card | null> {
    if (!params.token) {
      throw new InvalidArgumentError("No está autorizado", httpStatus.UNAUTHORIZED)
    }

    const currentToken = new TokenId(params.token)
    const tokenFound = await this.tokenRetriever.run(currentToken)

    if (!tokenFound) {
      throw new InvalidArgumentError("Token vencido")
    }
    return (await this.cardRepository.findByToken(currentToken))?.toPrimitives()
  }
}
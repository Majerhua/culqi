import {CardRepository} from "../../domain/CardRepository";
import {TokenCreator} from "../../../Token/application/create/TokenCreator";
import {TokenRemover} from "../../../Token/application/remove/TokenRemover";
import {CardNumber} from "../../domain/CardNumber";
import {TokenId} from "../../../Token/domain/TokenId";
import {Card} from "../../domain/Card";
import {CardCCV} from "../../domain/CardCCV";
import {CardExpirationMonth} from "../../domain/CardExpirationMonth";
import {CardExpirationYear} from "../../domain/CardExpirationYear";
import {CardEmail} from "../../domain/CardEmail";

export class CardTokenizer {
  private readonly cardRepository: CardRepository
  private readonly tokenCreator: TokenCreator
  private readonly tokenRemover: TokenRemover

  constructor(cardRepository: CardRepository, tokenCreator: TokenCreator, tokenRemover: TokenRemover) {
    this.cardRepository = cardRepository
    this.tokenCreator = tokenCreator
    this.tokenRemover = tokenRemover
  }

  async run(params: {
    card_number: string,
    cvv: string,
    expiration_month: string,
    expiration_year: string,
    email: string
  }): Promise<{ token: string }> {
    const card = new Card(
      "",
      new CardNumber(params.card_number),
      new CardCCV(params.cvv),
      new CardExpirationMonth(params.expiration_month),
      new CardExpirationYear(params.expiration_year),
      new CardEmail(params.email)
    )
    const currenCard = await this.cardRepository.findByCardNumber(new CardNumber(params.card_number))
    if (currenCard) {
      await this.tokenRemover.run(new TokenId(currenCard.token))
    }
    const token = await this.tokenCreator.run(JSON.stringify(params))
    if (currenCard) {
      await this.cardRepository.updateTokenByCardNumber(new CardNumber(params.card_number), new TokenId(token))
    } else {
      card.setToken(token)
      await this.cardRepository.save(card)
    }
    return {token: token}
  }
}
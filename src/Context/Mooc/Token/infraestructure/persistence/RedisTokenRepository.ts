import {TokenRepository} from "../../domain/TokenRepository";
import {Token} from "../../domain/Token";
import {TokenId} from "../../domain/TokenId";
import {RedisRepository} from "../../../../Shared/infraestructure/persistence/redis/RedisRepository";

export class RedisTokenRepository extends RedisRepository implements TokenRepository {
  async removeById(token: TokenId): Promise<void> {
    await this.delete(token.value)
  }

  async save(token: Token, value: string, expirationTime: number): Promise<void> {
    await this.set(token.value.value, value, expirationTime)
  }

  async getById(tokeId: TokenId): Promise<string | null> {
    return this.get(tokeId.value)
  }

  async end() {
    await this.close()
  }
}
import {TokenRepository} from "../../domain/TokenRepository";
import {TokenId} from "../../domain/TokenId";

export class TokenRetriever {
  private readonly tokenRepository: TokenRepository

  constructor(tokenRepository: TokenRepository) {
    this.tokenRepository = tokenRepository
  }

  async run(tokeId: TokenId): Promise<string | null> {
    return await this.tokenRepository.getById(tokeId)
  }

}
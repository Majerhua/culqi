import {Token} from "../../domain/Token";
import {TokenRepository} from "../../domain/TokenRepository";
import config from "../../../Shared/Config";

export class TokenCreator {
  private readonly tokenRepository
  private readonly TOKEN_DURATION = config.get("redis.token_duration")

  constructor(tokenRepository: TokenRepository) {
    this.tokenRepository = tokenRepository
  }

  async run(value: string): Promise<string> {
    const newToken: Token = Token.create(value)
    await this.tokenRepository.save(newToken, newToken.value.value, this.TOKEN_DURATION)
    return newToken.value.value
  }
}
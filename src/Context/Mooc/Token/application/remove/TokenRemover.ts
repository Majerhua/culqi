import {TokenRepository} from "../../domain/TokenRepository";
import {TokenId} from "../../domain/TokenId";

export class TokenRemover {
   private readonly tokenRepository

   constructor(tokenRepository: TokenRepository) {
      this.tokenRepository = tokenRepository
   }

   async run(tokenId: TokenId) {
      await this.tokenRepository.removeById(tokenId)
   }
}
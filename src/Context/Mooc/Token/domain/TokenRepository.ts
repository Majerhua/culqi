import {Token} from "./Token";
import {TokenId} from "./TokenId";

export interface TokenRepository {
  save(token: Token, value: string, expirationTime: number): Promise<void>;

  removeById(token: TokenId): Promise<void>;

  getById(tokeId: TokenId): Promise<string | null>;
}
import {TokenId} from "./TokenId";

export class Token {
   readonly value: TokenId
   private static readonly MAX_SIZE_SECRET = 12

   constructor(value: TokenId) {
      this.value = value
   }

   static create(body: string) {
      const secret = this.generateRandomCharacters(this.MAX_SIZE_SECRET)
      const newToken = this.sanitizeText(this.customEncrypt(body, secret))
      return new Token(new TokenId(newToken))
   }

   static fromPrimitive(plainDate: { value: string }): Token {
      return new Token(
         new TokenId(plainDate.value)
      )
   }

   private static sanitizeText(text: string) {
      return text.replace(/[^0-9a-zA-Z]/g, '');
   }

   private static generateRandomCharacters(length: number) {
      const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      let result = '';
      for (let i = 0; i < length; i++) {
         const randomIndex = Math.floor(Math.random() * characters.length);
         result += characters.charAt(randomIndex);
      }
      return result;
   }

   private static customEncrypt(input: string, secret: string) {
      return Buffer.from(this.shuffleString(input + secret)).toString('base64').slice(0, 16);
   }

   private static shuffleString(input: string) {
      const array = input.split('');
      for (let i = array.length - 1; i > 0; i--) {
         const j = Math.floor(Math.random() * (i + 1));
         [array[i], array[j]] = [array[j], array[i]];
      }
      return array.join('');
   }
}
import {StringValueObject} from "../../../Shared/domain/valueObject/StringValueObject";
import {InvalidArgumentError} from "../../../Shared/domain/valueObject/InvalidArgumentError";

export class TokenId extends StringValueObject {
  private readonly regexAlphaNum: RegExp = /^[a-zA-Z0-9]+$/
  private readonly size = 16

  constructor(value: string) {
    super(value);
    this.ensureSize(value)
    this.ensureThatHasStringAndNumbers(value)
  }

  ensureThatHasStringAndNumbers(value: string) {
    const response: boolean = this.regexAlphaNum.test(value)
    if (!response) {
      throw new InvalidArgumentError("Formato de token inválido")
    }
  }

  ensureSize(input: string) {
    if (input.length !== this.size) {
      throw new InvalidArgumentError("Tamaño de token inválido")
    }
  }
}
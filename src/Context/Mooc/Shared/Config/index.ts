import convict from 'convict';

const moocConfig = convict({
  env: {
    doc: 'The application environment.',
    format: ['dev', 'test', 'prod'],
    default: 'dev',
    env: 'NODE_ENV'
  },
  postgres: {
    host: {
      doc: 'The database host',
      format: String,
      env: 'POSTGRES_HOST',
      default: 'localhost'
    },
    port: {
      doc: 'The database port',
      format: Number,
      env: 'POSTGRES_PORT',
      default: 5432
    },
    username: {
      doc: 'The database username',
      format: String,
      env: 'POSTGRES_USERNAME',
      default: 'codely'
    },
    password: {
      doc: 'The database password',
      format: String,
      env: 'POSTGRES_PASSWORD',
      default: 'codely'
    },
    database: {
      doc: 'The database name',
      format: String,
      env: 'POSTGRES_DATABASE',
      default: 'mooc-backend-dev'
    }
  },
  redis: {
    host: {
      doc: 'Redis host',
      format: String,
      env: 'HOST_REDIS',
      default: 'localhost'
    },
    port: {
      doc: 'Redis port',
      format: Number,
      env: 'PORT_REDIS',
      default: 10710
    },
    password: {
      doc: 'Redis password',
      format: String,
      env: 'PASSWORD_REDIS',
      default: 'codely'
    },
    user: {
      doc: 'Redis user',
      format: String,
      env: 'USER_REDIS',
      default: 'codely'
    },
    token_duration: {
      doc: 'Redis token duration',
      format: Number,
      env: 'TOKEN_DURATION',
      default: 10710
    }
  }
});
moocConfig.loadFile([__dirname + '/example.dev.json', __dirname + '/env.' + moocConfig.get('env') + '.json']);
export default moocConfig;

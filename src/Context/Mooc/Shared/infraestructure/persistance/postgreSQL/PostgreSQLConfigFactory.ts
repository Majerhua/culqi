import {PostgreSQLConfig} from "../../../../../Shared/infraestructure/persistence/postgreSQL/PostgreSQLConfig";
import config from "../../../Config";

export class PostgreSQLConfigFactory {
  static createConfig(): PostgreSQLConfig {
    return {
      database: config.get('postgres.database'),
      host: config.get('postgres.host'),
      password: config.get('postgres.password'),
      port: config.get('postgres.port'),
      user: config.get('postgres.username'),
    }
  }
}
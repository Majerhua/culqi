import {RedisConfig} from "../../../../../Shared/infraestructure/persistence/redis/RedisConfig";
import config from "../../../Config";

export class RedisConfigFactory {
  static createConfig(): RedisConfig {
    const host = config.get('redis.host')
    const port = config.get('redis.port')
    const password = config.get('redis.password')
    const user = config.get('redis.user')
    return {url: `redis://${user}:${password}@${host}:${port}`}
  }
}
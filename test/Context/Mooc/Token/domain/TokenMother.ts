import {Token} from "../../../../../src/Context/Mooc/Token/domain/Token";
import {TokenIdMother} from "./TokenIdMother";
import {MotherCreator} from "../../../Shared/MotherCreator";

export class TokenMother {
  static random(): Token {
    return new Token(
      TokenIdMother.random()
    )
  }

  static invalidSize(size: number = 2): string {
    return MotherCreator.random().string.alpha(size)
  }

  static invalidFormat() {
    return MotherCreator.random().string.alpha(15) + "&"
  }
}
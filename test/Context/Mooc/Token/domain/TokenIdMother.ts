import {TokenId} from "../../../../../src/Context/Mooc/Token/domain/TokenId";
import {MotherCreator} from "../../../Shared/MotherCreator";

export class TokenIdMother {
  static create(value: string): TokenId {
    return new TokenId(value)
  }

  static random(): TokenId {
    return this.create(
      MotherCreator.random().string.alpha({length: 16})
    )
  }
}
import {TokenRemover} from "../../../../../../src/Context/Mooc/Token/application/remove/TokenRemover";
import {RedisTokenRepositoryMock} from "../../__mock__/RedisTokenRepositoryMock";
import {TokenMother} from "../../domain/TokenMother";

let tokenRepository: RedisTokenRepositoryMock
let tokenRemover: TokenRemover

beforeAll(() => {
  tokenRepository = new RedisTokenRepositoryMock()
  tokenRemover = new TokenRemover(tokenRepository)
})

describe("Eliminar token", () => {
  describe("Cuando el token existe", () => {
    it("Se elimina el token", async () => {
      const token = TokenMother.random()
      const response = await tokenRemover.run(token.value)
      tokenRepository.assertRemoveByIdHaveBeenCalledWith(token.value)
      expect(response).toBe(undefined)
    })
  })
})
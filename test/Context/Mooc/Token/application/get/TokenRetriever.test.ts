import {TokenRetriever} from "../../../../../../src/Context/Mooc/Token/application/get/TokenRetriever";
import {RedisTokenRepositoryMock} from "../../__mock__/RedisTokenRepositoryMock";
import {TokenMother} from "../../domain/TokenMother";

let tokenRepository: RedisTokenRepositoryMock
let tokenRetriever: TokenRetriever

beforeAll(() => {
  tokenRepository = new RedisTokenRepositoryMock()
  tokenRetriever = new TokenRetriever(tokenRepository)
})

describe("Obtener Token", () => {
  describe("El token aun existe", () => {
    it("debería retornar token", async () => {
      const token = TokenMother.random()
      tokenRepository.returnOnGetById(token)
      const response = await tokenRetriever.run(token.value)
      tokenRepository.assertGetByIdHaveBeenCalledWith(token.value)
      expect(response).toBe(token.value.value)
    })
  })
})
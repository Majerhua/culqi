import {TokenCreator} from "../../../../../../src/Context/Mooc/Token/application/create/TokenCreator";
import {Token} from "../../../../../../src/Context/Mooc/Token/domain/Token";
import {RedisTokenRepositoryMock} from "../../__mock__/RedisTokenRepositoryMock";
import {CardMother} from "../../../Card/domain/CardMother";

let tokenRepository: RedisTokenRepositoryMock
let tokenCreator: TokenCreator

beforeAll(() => {
  tokenRepository = new RedisTokenRepositoryMock()
  tokenCreator = new TokenCreator(tokenRepository)
})


describe("Crear nuevo token", () => {
  describe("Formato de token es correcto", () => {
    it("debería retornar un token", async () => {
      const value = CardMother.random().toPrimitives()
      const newToken = Token.create(JSON.stringify(value))
      jest.spyOn(Token, "create").mockReturnValue(newToken)
      const response = await tokenCreator.run(value)
      tokenRepository.assertSaveHaveBeenCalledWith(newToken, newToken.value.value, 900)
      expect(response).toBe(newToken.value.value)
    })
  })
})
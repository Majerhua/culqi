import {
  RedisTokenRepository
} from "../../../../../../src/Context/Mooc/Token/infraestructure/persistence/RedisTokenRepository";
import {RedisFactory} from "../../../../../../src/Context/Shared/infraestructure/persistence/redis/RedisFactory";
import {TokenId} from "../../../../../../src/Context/Mooc/Token/domain/TokenId";
import {
  RedisConfigFactory
} from "../../../../../../src/Context/Mooc/Shared/infraestructure/persistance/redis/RedisConfigFactory";
import {TokenMother} from "../../domain/TokenMother";

const redisTokenRepository = new RedisTokenRepository(RedisFactory._createClient(RedisConfigFactory.createConfig()))

afterAll(async () => {
  await redisTokenRepository.end()
})

describe("TokenRepository", () => {
  describe("#save", () => {
    it("Debería guardar token", async () => {
      const newToken = TokenMother.random()
      await redisTokenRepository.save(newToken, newToken.value.value, 200)
    })
  })

  describe("#getById", () => {
    it("Debería retornar el valor del token guardado", async () => {
      const newToken = TokenMother.random()
      await redisTokenRepository.save(newToken, newToken.value.value, 200)

      const response = await redisTokenRepository.getById(new TokenId(newToken.value.value))
      expect(response).toBe(newToken.value.value)
    })
  })

  describe("#removeById", () => {
    it("Debería borrar el token", async () => {
      const newToken = TokenMother.random()
      await redisTokenRepository.save(newToken, newToken.value.value, 200)
      await redisTokenRepository.removeById(new TokenId(newToken.value.value))
    })
  })
})



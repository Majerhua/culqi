import {TokenRepository} from "../../../../../src/Context/Mooc/Token/domain/TokenRepository";
import {TokenId} from "../../../../../src/Context/Mooc/Token/domain/TokenId";
import {Token} from "../../../../../src/Context/Mooc/Token/domain/Token"

export class RedisTokenRepositoryMock implements TokenRepository {
  private readonly getByIdMock: jest.Mock
  private readonly removeByIdMock: jest.Mock
  private readonly saveMock: jest.Mock
  private token: string | null

  constructor() {
    this.getByIdMock = jest.fn()
    this.removeByIdMock = jest.fn()
    this.saveMock = jest.fn()
  }


  async getById(tokeId: TokenId): Promise<string | null> {
    this.getByIdMock(tokeId)
    return this.token
  }

  returnOnGetById(token: Token | null) {
    this.token = token?.value?.value || null
  }

  async removeById(token: TokenId): Promise<void> {
    this.removeByIdMock(token)
  }

  assertRemoveByIdHaveBeenCalledWith(token: TokenId) {
    expect(this.removeByIdMock).toHaveBeenCalledWith(token)
  }

  async save(token: Token, value: string, expirationTime: number): Promise<void> {
    this.saveMock(token, value, expirationTime)
  }

  assertSaveHaveBeenCalledWith(token: Token, value: string, expirationTime: number) {
    expect(this.saveMock).toHaveBeenCalledWith(token, value, expirationTime)
  }

  assertGetByIdHaveBeenCalledWith(tokeId: TokenId) {
    expect(this.getByIdMock).toHaveBeenCalledWith(tokeId)
  }

}
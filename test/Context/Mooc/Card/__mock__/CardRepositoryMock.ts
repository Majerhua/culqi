import {CardRepository} from "../../../../../src/Context/Mooc/Card/domain/CardRepository";
import {CardNumber} from "../../../../../src/Context/Mooc/Card/domain/CardNumber";
import {Card} from "../../../../../src/Context/Mooc/Card/domain/Card";
import {TokenId} from "../../../../../src/Context/Mooc/Token/domain/TokenId";

export class CardRepositoryMock implements CardRepository {
  private readonly findByCardNumberMock: jest.Mock
  private readonly findByTokenMock: jest.Mock
  private readonly saveMock: jest.Mock
  private readonly updateTokenByCardNumberMock: jest.Mock
  private card: Card | null

  constructor() {
    this.findByCardNumberMock = jest.fn()
    this.findByTokenMock = jest.fn()
    this.saveMock = jest.fn()
    this.updateTokenByCardNumberMock = jest.fn()
  }


  async findByCardNumber(cardNumber: CardNumber): Promise<Card | null> {
    this.findByCardNumberMock(cardNumber)
    return this.card
  }

  returnOnFindByCardNumber(card: Card | null) {
    this.card = card
  }

  async findByToken(tokenId: TokenId): Promise<Card | null> {
    this.findByTokenMock(tokenId)
    return this.card
  }

  returnOnFindByToken(card: Card | null) {
    this.card = card
  }

  async save(card: Card): Promise<void> {
    this.saveMock(card)
  }

  assertSaveHaveBeenCalledWith(card: Card): void {
    expect(this.saveMock).toBeCalledWith(card)
  }

  async updateTokenByCardNumber(cardNumber: CardNumber, token: TokenId): Promise<void> {
    this.updateTokenByCardNumberMock(cardNumber, token)
  }

  assertUpdateTokenByCardNumberHaveBeenCalledWith(cardNumber: CardNumber, token: TokenId) {
    expect(this.updateTokenByCardNumberMock).toBeCalledWith(cardNumber, token)
  }

}
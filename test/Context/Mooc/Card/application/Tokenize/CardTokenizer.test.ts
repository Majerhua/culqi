import {TokenCreator} from "../../../../../../src/Context/Mooc/Token/application/create/TokenCreator";
import {TokenRemover} from "../../../../../../src/Context/Mooc/Token/application/remove/TokenRemover";
import {CardTokenizer} from "../../../../../../src/Context/Mooc/Card/application/Tokenize/CardTokenizer";
import {CardRepositoryMock} from "../../__mock__/CardRepositoryMock";
import {RedisTokenRepositoryMock} from "../../../Token/__mock__/RedisTokenRepositoryMock";
import {CardMother} from "../../domain/CardMother";
import {TokenMother} from "../../../Token/domain/TokenMother";
import {Token} from "../../../../../../src/Context/Mooc/Token/domain/Token";

let cardRepository: CardRepositoryMock
let tokenRepository: RedisTokenRepositoryMock
let tokenRemover: TokenRemover
let cardTokenizer: CardTokenizer
let tokenCreator: TokenCreator

beforeAll(() => {
  cardRepository = new CardRepositoryMock()
  tokenRepository = new RedisTokenRepositoryMock()
  tokenCreator = new TokenCreator(tokenRepository)
  tokenRemover = new TokenRemover(tokenRepository)
  cardTokenizer = new CardTokenizer(cardRepository, tokenCreator, tokenRemover)
})

describe("Tokenizar Tarjeta", () => {
  describe("Todos los datos están correctos", () => {
    it("debería retornar token", async () => {

      const card = CardMother.random()
      const token = TokenMother.random()

      const params = {
        card_number: card.card_number.value,
        cvv: card.ccv.value,
        expiration_month: card.expiration_month.value,
        expiration_year: card.expiration_year.value,
        email: card.email.value
      }

      jest.spyOn(Token, "create").mockReturnValue(token)
      cardRepository.returnOnFindByCardNumber(null)
      const response = await cardTokenizer.run(params)
      expect(response.token).toBe(token.value.value);
    })
  })

  describe("El número de tarjeta es invalido", () => {
    it("debería retornar un excepción con el mensaje (Número de tarjeta invalida)", async () => {
      const card = CardMother.createCardWithInvalidCardNumber()
      await expect(cardTokenizer.run(card)).rejects.toThrow('Número de tarjeta invalida');
    })
  })


  describe("El número de tarjeta tiene menos de 13 dígitos", () => {
    it("debería retornar un excepción con el mensaje (Número de dígitos tiene que ser mayor a 13 y menor a 16)", async () => {
      const card = CardMother.createCardWithInvalidCardNumberMaxSize()
      await expect(cardTokenizer.run(card)).rejects.toThrow('Número de dígitos tiene que ser mayor a 13 y menor a 16');
    })
  })


  describe("El cvv tiene un caracter extraño", () => {
    it("debería retornar un excepción con el mensaje (Cvv solo permite números)", async () => {
      const card = CardMother.createCardWithInvalidCharacterInCCV()
      await expect(cardTokenizer.run(card)).rejects.toThrow('Cvv solo permite números');
    })
  })

  describe("El número de caracteres del cvv está fuera de rango (3,4 caracteres)", () => {
    it("debería retornar un excepción con el mensaje (El tamaño de cvv tiene que ser entre 3 a 4 dígito)", async () => {
      const card = CardMother.createCardWithInvalidCCVOutRange()
      await expect(cardTokenizer.run(card)).rejects.toThrow('El tamaño de cvv tiene que ser entre 3 a 4 dígito');
    })
  })


  describe("Mes de expiración está fuera de rango (1,12)", () => {
    it("debería retornar un excepción con el mensaje (Mes de vencimiento tiene que estar en el rango de 1 a 12)", async () => {
      const card = CardMother.createCardWithInvalidExpirationMonthOutRange()
      await expect(cardTokenizer.run(card)).rejects.toThrow('Mes de vencimiento tiene que estar en el rango de 1 a 12');
    })
  })


  describe("Año de expiración es menor que el año actual", () => {
    it("debería retornar un excepción con el mensaje (Tarjeta vencida)", async () => {
      const card = CardMother.createCardWithInvalidExpiredYear()
      await expect(cardTokenizer.run(card)).rejects.toThrow('Tarjeta vencida');
    })
  })


  describe("Año de expiración es mayor en 5 años al año actual", () => {
    it("debería retornar un excepción con el mensaje (Año de vencimiento fuera de rango)", async () => {
      const card = CardMother.createCardWithInvalidYearForMoreFiveYear()
      await expect(cardTokenizer.run(card)).rejects.toThrow('Año de vencimiento fuera de rango');
    })
  })

  describe("El formato de correo es invalido", () => {
    it("debería retornar un excepción con el mensaje (Email tiene formato inválido)", async () => {
      const card = CardMother.createCardWithInvalidEmail()
      await expect(cardTokenizer.run(card)).rejects.toThrow('Email tiene formato inválido');
    })
  })

  describe("El formato de correo es gmail.com o hotmail.com o yahoo.es", () => {
    it("debería retornan token", async () => {

      const token = TokenMother.random()

      const card = CardMother.random()
      const params = {
        card_number: card.card_number.value,
        cvv: card.ccv.value,
        expiration_month: card.expiration_month.value,
        expiration_year: card.expiration_year.value,
        email: card.email.value
      }

      jest.spyOn(Token, "create").mockReturnValue(token)
      const response = await cardTokenizer.run(params)
      expect(response.token).toBe(token.value.value);
    })
  })
})


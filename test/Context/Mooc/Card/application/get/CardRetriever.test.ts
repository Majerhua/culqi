import {TokenRetriever} from "../../../../../../src/Context/Mooc/Token/application/get/TokenRetriever";
import {CardRetriever} from "../../../../../../src/Context/Mooc/Card/application/get/CardRetriever";
import {CardRepositoryMock} from "../../__mock__/CardRepositoryMock";
import {RedisTokenRepositoryMock} from "../../../Token/__mock__/RedisTokenRepositoryMock";
import {TokenMother} from "../../../Token/domain/TokenMother";
import {CardMother} from "../../domain/CardMother";

let cardRepository: CardRepositoryMock
let tokenRepository: RedisTokenRepositoryMock
let tokenRetriever: TokenRetriever
let cardRetriever: CardRetriever

beforeAll(() => {
  cardRepository = new CardRepositoryMock()
  tokenRepository = new RedisTokenRepositoryMock()
  tokenRetriever = new TokenRetriever(tokenRepository)
  cardRetriever = new CardRetriever(cardRepository, tokenRetriever)
})

describe("Consultar tarjeta", () => {
  describe("EL token aun existe en redis", () => {
    it("debería retornar la tarjeta sin ccv", async () => {
      const token = TokenMother.random()
      const card = CardMother.createCardWithCustomToken(token)
      tokenRepository.returnOnGetById(token)
      cardRepository.returnOnFindByToken(card)

      const response = await cardRetriever.run({
        token: token.value.value
      })

      tokenRepository.assertGetByIdHaveBeenCalledWith(token.value)

      expect(response).toMatchObject(
        {
          card_number: card.card_number.value,
          expiration_month: card.expiration_month.value,
          expiration_year: card.expiration_year.value,
          email: card.email.value
        });
    })
  })

  describe("EL token ya no existe en redis", () => {
    it("debería retornar una excepción con el mensaje (Token vencido)", async () => {
      const token = TokenMother.random()
      tokenRepository.returnOnGetById(null)
      await expect(cardRetriever.run({token: token.value.value})).rejects.toThrow('Token vencido');
    })
  })

  describe("El usuario no manda token en el berer", () => {
    it("debería retornar una excepción con el mensaje (Token vencido)", async () => {
      await expect(cardRetriever.run({token: null})).rejects.toThrow('No está autorizado');
    })
  })


  describe("Número de caracteres en token es menor a 16", () => {
    it("debería retornar una excepción con el mensaje (Token vencido)", async () => {
      const token = TokenMother.invalidSize(10)
      await expect(cardRetriever.run({token: token})).rejects.toThrow('Tamaño de token inválido');
    })
  })


  describe("Formato de token incorracto", () => {
    const token = TokenMother.invalidFormat()
    it("debería retornar una excepción con el mensaje (Formato de token inválido)", async () => {
      await expect(cardRetriever.run({token: token})).rejects.toThrow('Formato de token inválido');
    })
  })

})
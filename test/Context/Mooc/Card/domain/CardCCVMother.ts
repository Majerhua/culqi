import {CardCCV} from "../../../../../src/Context/Mooc/Card/domain/CardCCV";
import {MotherCreator} from "../../../Shared/MotherCreator";

export class CardCCVMother {
  static create(value: string): CardCCV {
    return new CardCCV(value)
  }

  static random(): CardCCV {
    return this.create(
      MotherCreator.random().finance.creditCardCVV()
    )
  }

  static invalidCharacter(): string {
    return "12b"
  }

  static outRange(): string {
    return "123554"
  }
}
import {MotherCreator} from "../../../Shared/MotherCreator";
import {CardExpirationYear} from "../../../../../src/Context/Mooc/Card/domain/CardExpirationYear";

export class CardExpirationYearMother {
  static create(value: string): CardExpirationYear {
    return new CardExpirationYear(value)
  }

  static random(): CardExpirationYear {
    return this.create(
      String(MotherCreator.random().date.future({years: 4}).getFullYear())
    )
  }

  static expired() {
    const nowYear = (new Date()).getFullYear()
    const pastYear = nowYear - 2
    return String(MotherCreator.random().date.past({
      refDate: (new Date()).setFullYear(pastYear),
      years: 4
    }).getFullYear())
  }

  static moreFiveYear() {
    const nowYear = (new Date()).getFullYear()
    const futureYear = nowYear + 6
    return String(MotherCreator.random().date.future({
      refDate: (new Date()).setFullYear(futureYear),
      years: 6
    }).getFullYear())
  }
}
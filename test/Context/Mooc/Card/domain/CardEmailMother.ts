import {CardEmail} from "../../../../../src/Context/Mooc/Card/domain/CardEmail";
import {MotherCreator} from "../../../Shared/MotherCreator";

export class CardEmailMother {
  static create(value: string): CardEmail {
    return new CardEmail(value)
  }

  static random(): CardEmail {
    const domain = MotherCreator.random().helpers.arrayElement(["gmail.com", "hotmail.com", "yahoo.es"])
    const userName = MotherCreator.random().internet.userName()
    return this.create(
      `${userName}@${domain}`
    )

  }

  static invalid(): string {
    const domain = MotherCreator.random().helpers.arrayElement(["gmail.ar", "hotmail.es", "yahoo.com"])
    const userName = MotherCreator.random().internet.userName()
    return `${userName}@${domain}`
  }
}
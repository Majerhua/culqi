import {Card} from "../../../../../src/Context/Mooc/Card/domain/Card";
import {CardNumberMother} from "./CardNumberMother";
import {CardCCVMother} from "./CardCCVMother";
import {CardExpirationYearMother} from "./CardExpirationYearMother";
import {CardExpirationMonthMother} from "./CardExpirationMonthMother";
import {CardEmailMother} from "./CardEmailMother";
import {Token} from "../../../../../src/Context/Mooc/Token/domain/Token"
import {TokenMother} from "../../Token/domain/TokenMother";

export class CardMother {
  static random(): Card {
    return new Card(
      "",
      CardNumberMother.random(),
      CardCCVMother.random(),
      CardExpirationMonthMother.random(),
      CardExpirationYearMother.random(),
      CardEmailMother.random()
    )
  }

  static createCardWithCustomToken(token: Token): Card {
    return new Card(
      token.value.value,
      CardNumberMother.random(),
      CardCCVMother.random(),
      CardExpirationMonthMother.random(),
      CardExpirationYearMother.random(),
      CardEmailMother.random()
    )
  }

  static createCardWithInvalidCardNumber(): any {
    return {
      card_number: CardNumberMother.invalid(),
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.random().value,
      email: CardEmailMother.random().value,
      expiration_month: CardExpirationMonthMother.random().value,
      expiration_year: CardExpirationYearMother.random().value
    }
  }

  static createCardWithInvalidCardNumberMaxSize(): any {
    return {
      card_number: CardNumberMother.invalidSize(),
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.random().value,
      email: CardEmailMother.random().value,
      expiration_month: CardExpirationMonthMother.random().value,
      expiration_year: CardExpirationYearMother.random().value
    }
  }

  static createCardWithInvalidCharacterInCCV(): any {
    return {
      card_number: CardNumberMother.random().value,
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.invalidCharacter(),
      email: CardEmailMother.random().value,
      expiration_month: CardExpirationMonthMother.random().value,
      expiration_year: CardExpirationYearMother.random().value
    }
  }

  static createCardWithInvalidCCVOutRange(): any {
    return {
      card_number: CardNumberMother.random().value,
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.outRange(),
      email: CardEmailMother.random().value,
      expiration_month: CardExpirationMonthMother.random().value,
      expiration_year: CardExpirationYearMother.random().value
    }
  }

  static createCardWithInvalidExpirationMonthOutRange(): any {
    return {
      card_number: CardNumberMother.random().value,
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.random().value,
      email: CardEmailMother.random().value,
      expiration_month: CardExpirationMonthMother.ourRange(),
      expiration_year: CardExpirationYearMother.random().value
    }
  }

  static createCardWithInvalidExpiredYear(): any {
    return {
      card_number: CardNumberMother.random().value,
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.random().value,
      email: CardEmailMother.random().value,
      expiration_month: CardExpirationMonthMother.random(),
      expiration_year: CardExpirationYearMother.expired()
    }
  }


  static createCardWithInvalidYearForMoreFiveYear(): any {
    return {
      card_number: CardNumberMother.random().value,
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.random().value,
      email: CardEmailMother.random().value,
      expiration_month: CardExpirationMonthMother.random(),
      expiration_year: CardExpirationYearMother.moreFiveYear()
    }
  }


  static createCardWithInvalidEmail(): any {
    return {
      card_number: CardNumberMother.random().value,
      token: TokenMother.random().value.value,
      cvv: CardCCVMother.random().value,
      email: CardEmailMother.invalid(),
      expiration_month: CardExpirationMonthMother.random(),
      expiration_year: CardExpirationYearMother.random()
    }
  }
}
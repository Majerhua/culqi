import {CardExpirationMonth} from "../../../../../src/Context/Mooc/Card/domain/CardExpirationMonth";
import {MotherCreator} from "../../../Shared/MotherCreator";

export class CardExpirationMonthMother {
  static create(value: string): CardExpirationMonth {
    return new CardExpirationMonth(value)
  }

  static random() {
    const monthNumber = MotherCreator.random().number.int({min: 1, max: 12});
    const formattedMonth = monthNumber.toString().padStart(2, '0');
    return this.create(
      String(formattedMonth)
    )
  }

  static ourRange(): string{
    return "123"
  }
}
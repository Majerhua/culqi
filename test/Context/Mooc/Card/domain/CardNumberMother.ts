import {CardNumber} from "../../../../../src/Context/Mooc/Card/domain/CardNumber";

export class CardNumberMother {
  static create(value: string): CardNumber {
    return new CardNumber(value)
  }

  static invalid() {
    return this.generateCreditCardNumber(true)
  }

  static invalidSize() {
    return this.generateCreditCardNumber(true).slice(0, 8)
  }

  static random(): CardNumber {
    return this.create(
      this.generateCreditCardNumber()
    )
  }

  static generateCreditCardNumber(invalid = false) {
    let cardNumber = '';

    // Genera los primeros 15 dígitos de manera aleatoria
    for (let i = 0; i < 15; i++) {
      cardNumber += Math.floor(Math.random() * 10).toString();
    }

    // Calcula el dígito de verificación utilizando el algoritmo de Luhn
    const digits = cardNumber.split('').map(Number);
    let sum = 0;
    if (invalid) {
      sum = 3
    }
    let isSecondDigit = true;

    for (let i = digits.length - 1; i >= 0; i--) {
      let digit = digits[i];

      if (isSecondDigit) {
        digit *= 2;
        if (digit >= 10) {
          digit = digit - 9;
        }
      }

      sum += digit;
      isSecondDigit = !isSecondDigit;
    }

    const checksumDigit = (10 - (sum % 10)) % 10;
    cardNumber += checksumDigit;

    return String(cardNumber);
  }
}


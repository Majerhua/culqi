import {
  PostgreSQLCardRepository
} from "../../../../../../src/Context/Mooc/Card/infraestructure/persistence/PostgreSQLCardRepository";
import {
  PostgreSQLFactory
} from "../../../../../../src/Context/Shared/infraestructure/persistence/postgreSQL/PostgreSQLFactory";
import {CardNumber} from "../../../../../../src/Context/Mooc/Card/domain/CardNumber";
import {TokenId} from "../../../../../../src/Context/Mooc/Token/domain/TokenId";
import {
  PostgreSQLConfigFactory
} from "../../../../../../src/Context/Mooc/Shared/infraestructure/persistance/postgreSQL/PostgreSQLConfigFactory";
import {TokenMother} from "../../../Token/domain/TokenMother";
import {CardMother} from "../../domain/CardMother";

const postgreSQLCardRepository = new PostgreSQLCardRepository(PostgreSQLFactory.createClient(PostgreSQLConfigFactory.createConfig()))

afterAll(async () => {
  await postgreSQLCardRepository.end()
});

describe("CardRepository", () => {
  describe("#save", () => {
    it("Debería guardar un tarjeta", async () => {
      const newToken = TokenMother.random()
      const newCard = CardMother.createCardWithCustomToken(newToken)
      await postgreSQLCardRepository.save(newCard)
    })
  })

  describe("#findByCardNumber", () => {
    it("Debería recuperar una tarjeta", async () => {
      const newToken = TokenMother.random()
      const newCard = CardMother.createCardWithCustomToken(newToken)
      await postgreSQLCardRepository.save(newCard)
      const cardFound = await postgreSQLCardRepository.findByCardNumber(new CardNumber(newCard.card_number.value))
      expect(cardFound).toBeTruthy()
    })
  })

  describe("#updateTokenByCardNumber", () => {
    it("Debería actualizar una tarjeta existente", async () => {
      const newToken = TokenMother.random()
      const newCard = CardMother.createCardWithCustomToken(newToken)
      await postgreSQLCardRepository.save(newCard)
      await postgreSQLCardRepository.updateTokenByCardNumber(new CardNumber(newCard.card_number.value), new TokenId(newToken.value.value))
    })
  })

  describe("#findByToken", () => {
    it("Debería recuperar una tarjeta", async () => {
      const newToken = TokenMother.random()
      const newCard = CardMother.createCardWithCustomToken(newToken)
      await postgreSQLCardRepository.save(newCard)
      const cardFound = await postgreSQLCardRepository.findByToken(newToken.value)
      expect(cardFound).toMatchObject(newCard)
    })
  })
})
# culqi

# EndPoinds

1) Para tokenizar tarjeta: POST  https://7wxar216d1.execute-api.us-east-2.amazonaws.com/dev/creditCard
```JSON
{
    "card_number": "4213550152576335",
    "email": "marcelino.majerhua@gmail.com",
    "expiration_year": "2025",
    "cvv": "123",
    "expiration_month": "12"
}
```

2) Para recuperar tarjeta: GET  https://7wxar216d1.execute-api.us-east-2.amazonaws.com/dev/creditCard

# Consideracion

1) El proyecto está enfocado la arquitectura hexagonal
2) Enfocado en DDD
3) La carpeta App contiene la configuración del servidor
4) La carpeta Context contiene la lógica de negocios
5) Handlers son los puntos de entrada (lambdas)

## Inicializar las bases de datos

1) PostgreSql: sudo docker run --name culqi-db -e POSTGRES_PASSWORD=culqi -p 5432:5432 -d postgres
2) Redis: sudo docker run -d --name redis -p 6379:6379 redis:latest --requirepass culqi

## Crear la tabla de tarjetas (CARD)

```sql
CREATE TABLE card (
    id SERIAL PRIMARY KEY,
    token VARCHAR(255) NOT NULL UNIQUE,
    card_number VARCHAR(16) NOT NULL,
    ccv VARCHAR(4) NOT NULL,
    expiration_month VARCHAR(2) NOT NULL,
    expiration_year VARCHAR(4) NOT NULL,
    email VARCHAR(255) NOT NULL
);
```

## Build project

npm run build

## Copiar archivo example.dev.json a env.dev.json

En la carpeta src/Context/Mooc/Shared/Config
las variables de entorno son reales solo para probar

## test

npm run test

## Ejecutar entorno local

1) Se tiene que tener serverless-offline para instalar: npm i -g serverless-offline
2) Ejecutar: npm run start:local

## Despliegue a AWS

Ejecutar: npm run deploy:dev
